<div align="left" style="margin-top: 1em; margin-bottom: 3em;">
  <a href="https://thefiremask.com"><img src="https://raw.githubusercontent.com/DonaSwap/donaswap.com/main/public/images/ecosystem/firemask-logo.png" alt="thefiremask.com" width="250"></a>
</div>

# FireMask

FireMask is a mobile wallet that provides easy access to websites that use the [Firechain](https://thefirechain.com/) blockchain.

For up to the minute news, follow our [Twitter](https://twitter.com/firemaskwallet) or [Medium](https://medium.com/firemaskwallet) pages.

To learn how to develop FireMask-compatible applications, visit our [Developer Docs](https://docs.thefiremask.com).

## Ecosystem REPOs

<div align="center" style="margin-top: 1em; margin-bottom: 3em;">
  <a href="https://github.com/donaswap"><img alt="donaswap logo" src="https://raw.githubusercontent.com/DonaSwap/donaswap.com/main/public/images/ecosystem/donaswap-logo.png" alt="donaswap.com" width="80"></a>
  <a href="https://github.com/punknights"><img alt="punkknights logo" src="https://raw.githubusercontent.com/DonaSwap/donaswap.com/main/public/images/ecosystem/punkknights-logo.png" alt="punkknights.com" width="80"></a>
  <a href="https://github.com/fireswapdex"><img alt="fireswap logo" src="https://raw.githubusercontent.com/DonaSwap/donaswap.com/main/public/images/ecosystem/fireswapdex-logo.png" alt="thefireswap.com" width="80"></a>
  <a href="https://github.com/fireswapdex"><img alt="fireflame logo" src="https://raw.githubusercontent.com/DonaSwap/donaswap.com/main/public/images/ecosystem/fireflame-logo.png" alt="thefireswap.com" width="80"></a>
  <a href="https://github.com/thefirechain"><img alt="firechain logo" src="https://raw.githubusercontent.com/DonaSwap/donaswap.com/main/public/images/ecosystem/firechain-logo.png" alt="thefirechain.com" width="80"></a>
  <a href="https://github.com/thefirescan"><img alt="firescan logo" src="https://raw.githubusercontent.com/DonaSwap/donaswap.com/main/public/images/ecosystem/firescan-logo.png" alt="thefirescan.com" width="80"></a>
  <a href="https://github.com/thefiresea"><img alt="firesea logo" src="https://raw.githubusercontent.com/DonaSwap/donaswap.com/main/public/images/ecosystem/firesea-logo.png" alt="thefiresea.com" width="80"></a>
  <a href="https://github.com/firestationcex"><img alt="firestation logo" src="https://raw.githubusercontent.com/DonaSwap/donaswap.com/main/public/images/ecosystem/firestationcex-logo.png" alt="firestationcex.com" width="80"></a>
</div>

<hr style="margin-top: 3em; margin-bottom: 3em;">

## Contributors

Thanks goes to these wonderful people ([emoji key](https://allcontributors.org/docs/en/emoji-key)):
